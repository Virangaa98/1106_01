#include<stdio.h>
int main(){
	int num,i,fact,totfact=0;

	printf("Enter a number : ");
	scanf("%d",&num);

	for(i=1;i<=num;i++){
		fact=num%i;
		if(fact==0){
			totfact+=1;
		}
	}
	printf("%d has %d factors\n",num,totfact);
	if(totfact<=2){
		printf("So %d is a prime number\n",num);
	}
	else{
		printf("So %d is not a prime number\n",num);
	}
	return 0;
}
